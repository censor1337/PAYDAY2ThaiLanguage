menu_controls= "การบังคับ",--CONTROLS
menu_controls_help= "ปรับเปลี่ยนการตั้งค่าปุ่มของคุณ",--Change your control preferences.
menu_video= "วิดีโอ",--VIDEO
menu_video_help= "ปรับเปลี่ยนการตั้งค่าวิดีโอของคุณ",--Change your video preferences.
menu_sound= "เสียง",--SOUND
menu_sound_help= "ปรับเปลี่ยนการตั้งค่าเสียงของคุณ",--Change your sound preferences.
menu_network= "เครือข่าย",--Network
menu_network_help= "ปรับเปลี่ยนการตั้งค่าเครือข่ายของคุณ",--Adjust network settings
menu_mods= "มอด",--Mods
menu_mods_help= "ตรวจสอบสถานะมอดที่ติดตั้ง",--Check status on installed mods
menu_option_default= "เรียกคืนการตั้งค่า",--DEFAULT OPTIONS
menu_option_default_help= "เรียกคืนการตั้งค่าดังเดิมทั้งหมด",--Resets all options to their default state.
menu_clear_progress= "ลบเซฟเกมทั้งหมด",--CLEAR PROGRESS
menu_clear_progress_help= "ลบเซฟเกมออกทั้งหมดเหมือนซื้อเกมใหม่",--This will reset your cash, reputation and upgrades.
menu_mods = "ส่วนเสริม",--Mods
menu_mods_help = "ส่วนเสริมของเกมที่ผู้เล่นสร้างขั้นเอง",--Check status on installed mods
base_options_menu_lua_mods = "ปิดหรือเปิดส่วนเสริม",--Lua Mod
base_options_menu_lua_mod_options = "ตั้งค่าของส่วนเสริม",--Mod option
base_options_menu_keybinds = "ตั้งค่าปุ่มของส่วนเสริม",--Mod Keybind
menu_ban_list = "รายชื่อผู้เล่นที่ถูกแบน",--Ban List
menu_quickplay_settings = "การตั้งค่าหาห้องรวดเร็ว",--QuickPlay Settings
menu_gameplay = "ระบบการเล่น",--
menu_gameplay_help = "ปรับแต่งระบบการเล่น",--
menu_user_interface = "ลักษณะของผู้เล่น",--
menu_user_interface_help = "ปรับแต่งลักษณะของเกมตามผู้เล่น",--
menu_loading_hints = "เทคนิคการเล่น",--
menu_loading_hints_help = "แสดงเทคนิคการเล่นขณะโหลดเกม",--
menu_throwable_contour = "เส้นขอบของอาวุธขว้าง",--
menu_ammo_contour = "เส้นขอบของกระสุน",--
menu_mute_heist_vo = "ปิดเสียงของผู้ว่าจ้าง",--
menu_throwable_contour_help = "แสดงเส้นขอบของอาวุธขว้าง",--
menu_ammo_contour_help = "แสดงเส้นขอบของกระสุน",--
menu_mute_heist_vo_help = "ปิดเสียงของผู้ว่าจ้างในงานจ้างต่างๆ",--
menu_toggle_workshop = "เปิดการใช้งาน Workshop",--
menu_toggle_workshop_help = "ปิดหรือเปิดการใช้งาน Workshop",--
menu_camera_sensitivity = "ความไวของมุมมอง",--LOOK SENSITIVITY
menu_camera_sensitivity_help = "ความไวของมุมมอง",--Increase or decrease the responsiveness of your look input.
menu_camera_zoom_sensitivity = "ความไวของมุมมองตอนเล็ง",--AIMING SENSITIVITY
menu_camera_zoom_sensitivity_help = "ความไวของมุมมองตอนเล็ง",--INCREASE OR DECREASE THE RESPONSIVENESS OF YOUR LOOK INPUT DURING AIMING.
menu_toggle_zoom_sensitivity = "ตั้งค่าการเล็งแบบเฉพาะ",--SEPARATE AIMING SETTING
menu_toggle_zoom_sensitivity_help = "เปิดใช้งานการตั้งค่าการตอบสนองของเมาส์ที่แยกจากกันในระหว่างการเล็ง",--Enable separate mouse responsiveness setting during aiming.
menu_invert_camera_vertically = "สลับแกน Y",--INVERT Y-AXIS
menu_invert_camera_vertically_help = "สลับแกน Y ของเมาส์",--Turn invert Y-Axis on or off.
menu_hold_to_steelsight = "กดค้างเพื่อเล็งปืน",--HOLD TO AIM
menu_hold_to_steelsight_help = "คลิกขวาค้างเพื่อเล็งปืนหากปิดการตั้งค่านี้จะคลิกครั้งเดียวเพื่อเล็งคลิกอีกทีเพื่อออกจากการเล็ง",--Turn hold button to aim through sights on or off.
menu_hold_to_run = "กดค้างเพื่อวิ่ง",--HOLD TO RUN
menu_hold_to_run_help = "คลิกชิฟค้างเพื่อวิ่งหากปิดการตั้งค่านี้จะกดครั้งเดียวเพื่อวิ่งกดอีกทีเพื่อเดิน",--Turn hold button to keep running on or off.
menu_hold_to_duck = "กดค้างเพื่อนั่ง",--HOLD TO CROUCH
menu_hold_to_duck_help = "คลิกคอนโทรค้างเพื่อนั่งหากปิดการตั้งค่านี้จะกดครั้งเดียวเพื่อนั่งกดอีกทีเพื่อยืน",--Turn hold button to keep crouching on or off.
menu_customize_controller = "ตั้งค่าปุ่มกด",--EDIT KEYS
menu_customize_controller_help = "ตั้งค่าปุ่มกดของคุณ",--Choose your preferred keyboard and mouse layout.
menu_control_option_default = "คืนการตั้งค่าดังเดิม",--DEFAULT CONTROL OPTIONS
menu_control_option_default_help = "เรียกคืนการตั้งค่าดังเดิมในส่วนของการควบคุม",--Resets control options to their default state.



menu_controller_normal= "ขณะเดิน",--On foot
menu_controller_type= "การควบคุม",--Controls
menu_button_move_forward= "เดินไปข้างหน้า",--Move forward
menu_button_move_back= "เดินกลับ",--Move back
menu_button_move_left= "เดินไปข้างซ้าย",--Move left (strafe)
menu_button_move_right= "เดินไปข้างขวา",--Move right (strafe)
menu_button_fire_weapon= "ยิงปืน",--Fire Weapon
menu_button_aim_down_sight= "เล็งปืน",--Aim Down Sight
menu_button_weapon_slot1= "ปืนรอง",--Secondary
menu_button_weapon_slot2= "ปืนหลัก",--Primary
menu_button_switch_weapon= "สลับปืน",--Switch Weapon
menu_button_reload= "เปลี่ยนกระสุนปืน",--Reload
menu_button_weapon_gadget= "เปิด/ปิดอุปกรณ์เพิ่มเติม",--Gadget on/off
menu_button_weapon_firemode= "เปลี่ยนรูปแบบการยิงของอาวุธ",--Weapon fire mode SINGLE/AUTO
menu_button_throw_grenade= "ขว้างระเบิด",--Throw grenade
menu_button_sprint_WIN32= "วิ่ง",--(press) Run
menu_button_sprint= "วิ่ง",--(press) Run
menu_button_jump= "กระโดด",--Jump
menu_button_crouch= "นั่ง",--Crouch
menu_button_melee_WIN32= "ใช้อาวุธระยะประชิต",--Melee Attack
menu_button_melee= "ใช้อาวุธระยะประชิต",--(press) Melee Attack
menu_button_shout= "ชี้เป้าหมาย",--Shout/Interact
menu_button_deploy= "วางอุปกรณ์เสริม",--Deploy Equipment/Throw Bag
menu_button_chat_message= "ข้อความแชท",--Chat message
menu_button_push_to_talk= "ใช้ไมค์",--Push to talk
menu_button_cash_inspect= "ตรวจสอบอาวุธ",--Inspect weapon
menu_button_deploy_bipod= "ใช้งานขาตั้งปืน",--Deploy bipod
menu_set_default_controller= "เรียกคืนการตั้งค่าดังเดิม",--USE DEFAULTS
menu_controller_vehicle= "ขณะใช้งานยานพาหนะ",--In vehicle
menu_button_accelerate= "ขับไปข้างหน้า",--Accelerate
menu_button_brake= "เบรค/ถอยหลัง",--Brake/Reverse
menu_button_turn_left= "เลี้ยวซ้าย",--Turn left
menu_button_turn_right= "เลี้ยวขวา",--Turn right
menu_button_handbrake= "ใช้เบรคมือ",--Handbrake
menu_button_vehicle_change_camera= "เปลี่ยนมุมมอง",--Change camera mode
menu_button_vehicle_rear_camera= "ดูข้างหลัง",--Look behind
menu_button_vehicle_shooting_stance= "ดูข้างหน้ากันชน",--Passenger shooting pose
menu_button_vehicle_exit= "เข้า/ออก จากยานพาหนะ",--Exit vehiclemenu_packet_throttling = "การควบคุมปริมาณข้อมูล",--Packet Throttling
menu_packet_throttling_help = "การควบคุมปริมาณการส่งข้อมูลแต่จะทำให้เซิฟเวอร์แลคหรือตอบสนองช้าลง",--Network packets will be sent in groups to reduce the network load, but increase delays.
menu_net_forwarding = "ใช้งานการฟอเวิดพอต",--Forwarding
menu_net_forwarding_help = "ใช้งานการฟอเวิดพอตเมื่อเชิ่มต่อโดยตรงกับผู้เล่นอื่นไม่ได้",--Network packets may be sent through other players if direct connection fails.
menu_net_use_compression = "ใช้งานการบีบอัดข้อมูล",--Use Compression
menu_net_use_compression_help = "ใช้งานการบีบอัดให้เกมลดการใช้อินเตอร์เน็ตน้อยลง",--Compresses network data before sending which reduces the network load, but makes the game slightly heavier.
menu_network_option_default = "คืนการตั้งค่าดังเดิม",--DEFAULT NETWORK OPTIONS
menu_network_option_default_help = "เรียกคืนการตั้งค่าดังเดิมในส่วนของเน็ตเวิร์ค",--Resets network options to their default state.menu_music_volume = "ค่าเสียงเพลง",--MUSIC VOLUME
menu_music_volume_help = "ความดังเสียงเพลงประกอบเกม",--Adjust the music volume.
menu_sfx_volume = "ค่าเสียงประกอบ",--SFX VOLUME
menu_sfx_volume_help = "ค่าเสียงประกอบเช่นยิงกระสุนปืน,เสียงแกะสลักของระเบิด,เสียงประตู",--Adjust the sound effects volume.
menu_voice_volume = "ค่าเสียงไมค์",--VOICE CHAT VOLUME
menu_voice_volume_help = "ค่าเสียงไมค์ของผู้เล่น",--Adjust the voice chat volume.
menu_voicechat_toggle = "ใช้งานระบบพูดคุยไมค์",--USE VOICE CHAT
menu_voicechat_toggle_help = "ใช้งานระบบพูดคุยไมค์ในทีม",--TURN VOICE CHAT ON OR OFF.
menu_push_to_talk_toggle = "ใช้งานระบบกดเพื่อพูด",--USE PUSH TO TALK
menu_push_to_talk_toggle_help = "ใช้งานระบบกดเพื่อพูด",--TURN PUSH TO TALK ON OR OFF.
menu_jukebox_heist_playlist = "กำหนดเพลย์ลิสต์ขณะปฏิบัติการ",--Custom heist Playlist
menu_jukebox_heist_playlist_help = "กำหนดเพลย์ลิสต์ขณะปฏิบัติการ",--Create a custom playlist with your favorite tracks.
menu_jukebox_heist_tracks = "กำหนดแทร็กขณะปฏิบัติการ",--Custom heist tracks
menu_jukebox_heist_tracks_help = "กำหนดแทร็กเพลงขณะปฎิบัติการ",--Choose a specific track for each heist.
menu_jukebox_menu_playlist = "กำหนดเพลย์ลิสต์ขณะอยู่หน้าเมนู",--Custom menu playlist
menu_jukebox_menu_playlist_help = "กำหนดเพลย์ลิสต์ขณะอยู่หน้าเมนูของเกม",--Create a custom playlist with your favorite menu tracks.
menu_jukebox_menu_tracks = "กำหนดแทร็กขณะอยู่หน้าเมนู",--Custom menu tracks
menu_jukebox_menu_tracks_help = "กำหนดแทร็กเพลงที่ควรเล่นเมื่ออยู่หน้าเมนู",--Choose what track should play in each menu screen.
menu_sound_option_default = "คืนการตั้งค่าดังเดิม",--DEFAULT SOUND OPTIONS
menu_sound_option_default_help = "เรียกคืนการตั้งค่าดังเดิมในส่วนของเสียง",--Resets sound options to their default state.menu_vsync = "วีซิงค์",--Vsync
menu_vsync_help = "ฟิคเฟรมเรทของเกม",--Turn vsync on or off. Vsync reduces tearing but caps framerate to the refresh rate of your screen.
menu_toggle_dof = "ระยะที่ให้ภาพคมชัด",--Depth of Field
menu_toggle_dof_help = "ระยะที่ให้ภาพคมชัดจากจุดที่เราโฟกัสลึกเข้าไปด้านหลังและด้านหน้า",--Turn Depth of Field on or off.
menu_toggle_gpu_flush = "ประมวลผลด้านกราฟฟิก",--Flush GPU
menu_toggle_gpu_flush_help = "ประมวลผลด้านกราฟฟิกจะต้องใช้ทรัพยากรจาก CPU",--Fixes render lag (can be perceived as mouse/controller lag). Can have minor impact on performance.
menu_use_thq_weapon_parts = "ใช้การเรนเดอร์อาวุธคุณภาพสูง",--Use HQ weapons
menu_use_thq_weapon_parts_help = "การเรนเดอร์อาวุธคุณภาพสูง",--Set to see other players weapons in high quality.
menu_texture_quality = "ความคมชัดของพื้นผิว",--TEXTURE QUALITY
menu_texture_quality_help = "ปรับเปลี่ยนความคมชัดของพื้นผิว",--Change the level of detail of the textures. Low increases performance, High increases visual quality.
menu_texture_quality_very_low = "ต่ำมาก",--Very Low
menu_texture_quality_low = "ต่ำ",--Low
menu_texture_quality_medium = "ปานกลาง",--Medium
menu_texture_quality_high = "สูง",--High
menu_texture_quality_very_high = "สูงมาก",--Very High
menu_shadow_quality = "คุณภาพเงา",--Shadow Quality
menu_shadow_quality_help = "ปรับเปลี่ยนคุณภาพเงา",--Change the level of detail of the shadow textures. Low increases performance, high increases visual quality.
menu_anim_lod = "คุณภาพการเคลื่อนไหว",--ANIMATION QUALITY
menu_anim_lod_help = "คุณภาพการเคลื่อนไหวหรือคุณภาพของอนิเมชั่นต่างๆ",--Change the level of detail of the animations. Low increases performance, High increases visual quality.
menu_anim_lod_1 = "ต่ำ",--Low
menu_anim_lod_2 = "ปานกลาง",--Medium
menu_anim_lod_3 = "สูง",--High
menu_fps_limit = "อัตราค่าเฟรมเรท",--FRAME RATE LIMIT
menu_fps_limit_help = "ปรับเฟรมเรทให้ดูลื่นไหลขึ้น",--Specify the maximum frame rate in frames per second. Use lower values to Decrease Overheating & Mouse Lag.
menu_lightfx = "เอฟเฟคแสง",--ALIENWARE LIGHTFX
menu_lightfx_help = "เอฟเฟคแสงของอุปกรณ์ของ Alienware",--Turn Alienware LightFX on or off, make sure your Alienware Command Center drivers are up to date.
menu_fov_adjustment = "มุมมองของปืน",--FOV adjustment
menu_fov_adjustment_help = "ปรับให้มุมมองของปืนให้มองได้ไกล/ใกล้ขึ้น",--Adjusts the width of the player field of view.
menu_headbob = "การเร่งความเร็วของกล้อง",--Use Acceleration Camera Effect
menu_headbob_help = "ปิด/เปิด เอฟเฟคส่ายหัวเวลาวิ่ง",--Turn acceleration camera effect on or off. Head bobbing makes your camera shake when walking/running.
menu_max_streaming_chunk = "การดาวน์โหลดข้อมูล",--Streaming Chunk KB
menu_max_streaming_chunk_help = "การดาวน์โหลดข้อมูลของแมพ",--The maximum amount of data per read operation the game will access synchronously from HDD.
menu_aa = "การลดรอยหยัก",--Anti aliasing
menu_aa_off = "ปิด",--Off
menu_ao = "คุณภาพสิ่งแวดล้อม",--Ambient Occlusion
menu_ao_off = "ปิด",--Off
menu_ao_help = "AOB จะทำให้ประสิทธิภาพความไหลลื่นสูงขึ้นแต่จะทำให้มุมมองประสิทธิภาพต่ำลง. SSAO สูงจะทำให้มุมมองมีประสิทธิภาพมากขึ้น",--AOB has higher performance but lower visual quality. SSAO has higher visual quality.
menu_ao_ssao_high = "SSAO สูง",--SSAO High
menu_ao_aob = "AOB",--AOB
menu_ao_ssao_low = "SSAO ต่ำ",--SSAO Low
menu_parallax = "ทำแผนที่ Parallax",--Parallax mapping
menu_anisotropic = "ความละเอียดของภาพระยะไกล",--ANISOTROPIC FILTERING
menu_corpse_limit = "ตั้งค่าขีดจำกัดของศพ",--Corpse Limit
menu_limit_low = "ต่ำ",--
menu_limit_normal = "ปกติ",--
menu_limit_increased = "สูง",--
menu_limit_high = "สูงขึ้น",--
menu_limit_very_high = "สูงมาก",--
menu_limit_ultra = "สูงสุด",--
menu_limit_unlimited = "ไม่จำกัด",--
menu_toggle_chromatic = "ความคลาดเคลื่อนสี",--
menu_toggle_chromatic_help = "เปิดหรือปิดการป้องกันความคลาดเคลื่อนของสี",--
menu_colorgrading = "โหมดของสี",--
menu_color_off = "ปิดการใช้งาน",--
menu_color_default = "ค่าพื้นฐาน",--
menu_colorgrading_help = "ปรับเปลี่ยนโหมดของสี",--
menu_corpse_limit_help = "ปรับแต่งจำนวนของศพที่ทิ้งไว้ในแผนที่ยิ่งมากเท่าไหร่ยิ่งใช้ทรัพยากรมากขึ้นเท่านั้น",--
menu_parallax_help = "ปิดหรือเปิดการเชื่อมแผนที่แบบ Parallax",--
menu_anisotropic_help = "ปรับการลดรอยหยักการประมานภาพในระยะไกล",--
menu_aa_help = "FXAA ประสิทธิภาพสูงกว่าแต่วิสัยทัศน์ต่ำลง SMAA ประสิทธิภาพต่ำกว่าแต่วิสัยทัศน์สูงกว่า",--
